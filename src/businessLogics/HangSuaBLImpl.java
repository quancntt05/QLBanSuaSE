package businessLogics;

import org.hibernate.Session;

import javaBeans.HangSua;
import util.HibernateUtil;

public class HangSuaBLImpl implements HangSuaBL{

	@Override
	public HangSua findHangSuaById(String id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		HangSua hangSua = session.get(HangSua.class, id);
		session.close();
		return hangSua;
	}
	
}
