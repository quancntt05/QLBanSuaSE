package businessLogics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javaBeans.CtHoadon;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;
import util.HibernateUtil;

public class SuaBLImpl implements SuaBL{

	@Override
	public List<Sua> dsSua() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		query.from(Sua.class);
		List<Sua> list = session.createQuery(query).getResultList();
		session.close();
		return list;
	}

	@Override
	public List<HangSua> dsHangSua() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<HangSua> query = builder.createQuery(HangSua.class);
		
		query.from(HangSua.class);
		List<HangSua> list = session.createQuery(query).getResultList();
		session.close();
		return list;
	}

	@Override
	public List<LoaiSua> dsLoaiSua() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<LoaiSua> query = builder.createQuery(LoaiSua.class);
		
		query.from(LoaiSua.class);
		List<LoaiSua> list = session.createQuery(query).getResultList();
		session.close();
		return list;
	}

	@Override
	public List<Sua> findSuaByKey(LoaiSua loaiSua, HangSua hangSua, String key) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> root = query.from(Sua.class);
		Predicate sql1 = builder.equal(root.get("loaiSua"), loaiSua);
		Predicate sql2 = builder.equal(root.get("hangSua"), hangSua);
		Predicate sql3 = builder.like(root.get("tenSua").as(String.class), "%"+key+"%");
		query.where(builder.and(sql1,sql2,sql3));

		List<Sua> list = session.createQuery(query).getResultList();
		session.close();
		return list;
	}

	@Override
	public Sua findSuaById(String id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Sua result = session.get(Sua.class, id);
		
		session.close();
		return result;
	}

	@Override
	public List<Sua> dsSuaBanChay() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
		
		Root<Sua> rootSua = query.from(Sua.class);
		Root<CtHoadon> rootCtHoaDon = query.from(CtHoadon.class);
		query.multiselect(rootSua,builder.sum(rootCtHoaDon.<Integer>get("soLuong")))
			.where(builder.equal(rootSua.get("maSua"), rootCtHoaDon.get("sua")))
			.groupBy(rootSua.get("maSua"))
			.orderBy(builder.desc(builder.sum(rootCtHoaDon.<Integer>get("soLuong"))));
		
		List<Object[]> listObj = session.createQuery(query).setMaxResults(5).getResultList();
		
		List<Sua> list = new ArrayList<>();
		for (Object[] Obj : listObj) {
			list.add((Sua) Obj[0]);
		}
		session.close();
		return list;
	}

	@Override
	public int themSuaMoi(Sua sua) {
		int result = 0;
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction transaction = session.beginTransaction();
		session.save(sua);
		try {
			transaction.commit();
			result = 1;
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return result;
	}

	@Override
	public List<Sua> dsSuaTheoLoaiSua(String maLoaiSua) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> rootSua = query.from(Sua.class);
		Root<LoaiSua> rootLoaiSua = query.from(LoaiSua.class);
		
		Predicate sql1 = builder.equal(rootSua.get("loaiSua"), rootLoaiSua.get("maLoaiSua"));
		Predicate sql2 = builder.equal(rootLoaiSua.get("maLoaiSua"), maLoaiSua);
		
		query.select(rootSua)
			.where(builder.and(sql1,sql2));
		
		List<Sua> list = session.createQuery(query).getResultList();
		
		session.close();
		return list;
	}

	@Override
	public List<Sua> dsSuaTheoHangSua(String maHangSua) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> rootSua = query.from(Sua.class);
		Root<HangSua> rootHangSua = query.from(HangSua.class);
		
		Predicate sql1 = builder.equal(rootSua.get("hangSua"), rootHangSua.get("maHangSua"));
		Predicate sql2 = builder.equal(rootHangSua.get("maHangSua"), maHangSua);
		
		query.select(rootSua)
			.where(builder.and(sql1,sql2));
		
		List<Sua> list = session.createQuery(query).getResultList();
		
		session.close();
		return list;
	}

	@Override
	public List<Sua> dsSuaTheoPhanTrang(List<Sua> dsSua, int start, int limit) {
		List<Sua> list = new ArrayList<>();
		if (dsSua.size() == 0) {
			return list;
		}
		for (int i = start; i < start+limit; i++) {
			list.add(dsSua.get(i));
		}
		return list;
	}

}
