package javaBeans;
// Generated Mar 25, 2018 7:46:33 PM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class KhachHang.
 * @see javaBeans.KhachHang
 * @author Hibernate Tools
 */
@Stateless
public class KhachHangHome {

	private static final Log log = LogFactory.getLog(KhachHangHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(KhachHang transientInstance) {
		log.debug("persisting KhachHang instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(KhachHang persistentInstance) {
		log.debug("removing KhachHang instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public KhachHang merge(KhachHang detachedInstance) {
		log.debug("merging KhachHang instance");
		try {
			KhachHang result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public KhachHang findById(String id) {
		log.debug("getting KhachHang instance with id: " + id);
		try {
			KhachHang instance = entityManager.find(KhachHang.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
