package businessLogics;

import javaBeans.HangSua;

public interface HangSuaBL {
	HangSua findHangSuaById(String id);
}
