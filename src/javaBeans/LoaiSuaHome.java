package javaBeans;
// Generated Mar 25, 2018 7:46:33 PM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class LoaiSua.
 * @see javaBeans.LoaiSua
 * @author Hibernate Tools
 */
@Stateless
public class LoaiSuaHome {

	private static final Log log = LogFactory.getLog(LoaiSuaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(LoaiSua transientInstance) {
		log.debug("persisting LoaiSua instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(LoaiSua persistentInstance) {
		log.debug("removing LoaiSua instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public LoaiSua merge(LoaiSua detachedInstance) {
		log.debug("merging LoaiSua instance");
		try {
			LoaiSua result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public LoaiSua findById(String id) {
		log.debug("getting LoaiSua instance with id: " + id);
		try {
			LoaiSua instance = entityManager.find(LoaiSua.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
