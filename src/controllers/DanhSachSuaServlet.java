package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.Pagination;
import businessLogics.SuaUtils;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/DanhSachSuaServlet")
public class DanhSachSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DanhSachSuaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        List<Sua> dsSua;
        
        if (request.getParameter("maHang") != null) {
        	dsSua = SuaUtils.getDao().dsSuaTheoHangSua(request.getParameter("maHang"));
        	
		}else if (request.getParameter("maLoai") != null) {
			dsSua = SuaUtils.getDao().dsSuaTheoLoaiSua(request.getParameter("maLoai"));
		}else {
	        dsSua = SuaUtils.getDao().dsSua();
		}
        List<HangSua> dsHangSua = SuaUtils.getDao().dsHangSua();
        List<LoaiSua> dsLoaiSua = SuaUtils.getDao().dsLoaiSua();
        
        request.setAttribute("dsHangSua", dsHangSua);
        request.setAttribute("dsLoaiSua", dsLoaiSua);
        
        //Pagination
        int totalItem = dsSua.size();
        int limit = 8, page = 1;
        
        if (request.getParameter("trang") != null) {
        	page = Integer.parseInt(request.getParameter("trang"));
		}
        
        Pagination pagination = new Pagination();
        int start = pagination.findStart(limit, page);
        int totalPages = pagination.findTotalPages(totalItem, limit);
        
        if (page == totalPages && totalItem-((totalPages-1)*limit) < limit) {
			limit = totalItem%limit;
		}
        
        List<Sua> dsSuaTheoPhanTrang = SuaUtils.getDao().dsSuaTheoPhanTrang(dsSua, start, limit);
        
        request.setAttribute("tongSoTrang", totalPages);
        request.setAttribute("dsSua", dsSuaTheoPhanTrang);

        request.getRequestDispatcher("views/danh-sach-sua.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
