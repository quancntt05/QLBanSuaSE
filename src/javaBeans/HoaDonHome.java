package javaBeans;
// Generated Mar 25, 2018 7:46:33 PM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class HoaDon.
 * @see javaBeans.HoaDon
 * @author Hibernate Tools
 */
@Stateless
public class HoaDonHome {

	private static final Log log = LogFactory.getLog(HoaDonHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(HoaDon transientInstance) {
		log.debug("persisting HoaDon instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(HoaDon persistentInstance) {
		log.debug("removing HoaDon instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public HoaDon merge(HoaDon detachedInstance) {
		log.debug("merging HoaDon instance");
		try {
			HoaDon result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HoaDon findById(String id) {
		log.debug("getting HoaDon instance with id: " + id);
		try {
			HoaDon instance = entityManager.find(HoaDon.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
