package businessLogics;

import org.hibernate.Session;

import javaBeans.LoaiSua;
import util.HibernateUtil;

public class LoaiSuaBLImpl implements LoaiSuaBL{

	@Override
	public LoaiSua findLoaiSuaById(String id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		LoaiSua loaiSua = session.get(LoaiSua.class, id);
		session.close();
		return loaiSua;
	}
	
}
