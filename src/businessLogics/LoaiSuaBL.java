package businessLogics;

import javaBeans.LoaiSua;

public interface LoaiSuaBL {
	LoaiSua findLoaiSuaById(String id);
}
