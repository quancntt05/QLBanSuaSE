package javaBeans;
// Generated Mar 25, 2018 7:46:33 PM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class HangSua.
 * @see javaBeans.HangSua
 * @author Hibernate Tools
 */
@Stateless
public class HangSuaHome {

	private static final Log log = LogFactory.getLog(HangSuaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(HangSua transientInstance) {
		log.debug("persisting HangSua instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(HangSua persistentInstance) {
		log.debug("removing HangSua instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public HangSua merge(HangSua detachedInstance) {
		log.debug("merging HangSua instance");
		try {
			HangSua result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HangSua findById(String id) {
		log.debug("getting HangSua instance with id: " + id);
		try {
			HangSua instance = entityManager.find(HangSua.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
