package businessLogics;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javaBeans.KhachHang;
import util.HibernateUtil;

public class KhachHangBLImpl implements KhachHangBL {

	@Override
	public int themMoiKH(KhachHang khachHang) {
		int result = 0; 
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction transaction = session.beginTransaction();
		session.save(khachHang);
		
		try {
			transaction.commit();
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

}
