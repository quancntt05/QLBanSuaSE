package businessLogics;

public class Pagination {
	public int findStart(int limit, int selectedPage) {
		int start = 0;
		if(selectedPage > 1) {
			start = (selectedPage - 1) * limit;
		}
		return start;
	}
	
	public int findTotalPages(int tongSanPham, int limit) {
		return (tongSanPham%limit == 0)? tongSanPham/limit : Math.floorDiv(tongSanPham, limit)+1;
	}
}
