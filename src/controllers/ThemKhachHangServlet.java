package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.HangSuaUtils;
import businessLogics.KhachHangUtils;
import businessLogics.LoaiSuaUtils;
import businessLogics.SuaUtils;
import javaBeans.HangSua;
import javaBeans.KhachHang;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/ThemKhachHangServlet")
public class ThemKhachHangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ThemKhachHangServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        List<HangSua> dsHangSua = SuaUtils.getDao().dsHangSua();
        List<LoaiSua> dsLoaiSua = SuaUtils.getDao().dsLoaiSua();

       
        request.setAttribute("loaiSua", dsLoaiSua);
        request.setAttribute("hangSua", dsHangSua);
        
        if(request.getParameter("btnThemMoi") != null) {
            String maKH = request.getParameter("txtMaKH");
            String tenKH = request.getParameter("txtTenKH");
            int phai = Integer.parseInt(request.getParameter("rdbPhai"));
            String diaChi = request.getParameter("txtDiaChi");
            String dienThoai = request.getParameter("txtDienThoai");
            String email = request.getParameter("txtEmail");
            
            
            KhachHang khachHang = new KhachHang(maKH, tenKH, phai, diaChi, dienThoai, email);
            int result = KhachHangUtils.getDAO().themMoiKH(khachHang);
            request.setAttribute("result", result);
        }        
        
        request.getRequestDispatcher("views/them-khach-hang.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
