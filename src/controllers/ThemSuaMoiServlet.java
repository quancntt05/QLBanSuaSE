package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.HangSuaUtils;
import businessLogics.KhachHangUtils;
import businessLogics.LoaiSuaUtils;
import businessLogics.SuaUtils;
import javaBeans.HangSua;
import javaBeans.KhachHang;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/ThemSuaMoiServlet")
public class ThemSuaMoiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ThemSuaMoiServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        List<HangSua> dsHangSua = SuaUtils.getDao().dsHangSua();
        List<LoaiSua> dsLoaiSua = SuaUtils.getDao().dsLoaiSua();

       
        request.setAttribute("loaiSua", dsLoaiSua);
        request.setAttribute("hangSua", dsHangSua);
        
        if(request.getParameter("btnThemMoi") != null) {
            String maSua = request.getParameter("txtMaSua");
            String tenSua = request.getParameter("txtTenSua");
            String hangSua = request.getParameter("cboHangSua");
            String loaiSua = request.getParameter("cboLoaiSua");
            int trongLuong = Integer.parseInt(request.getParameter("txtTrongLuong"));
            int donGia = Integer.parseInt(request.getParameter("txtDonGia"));
            String tpDinhDuong = request.getParameter("txtTPDinhDuong");
            String loiIch = request.getParameter("txtLoiIch");
            String hinh = request.getParameter("txtHinh");
            
            HangSua hangSua_ = HangSuaUtils.getDao().findHangSuaById(hangSua);
            LoaiSua loaiSua_ = LoaiSuaUtils.getDao().findLoaiSuaById(loaiSua);
            Sua sua = new Sua(maSua, hangSua_, loaiSua_, tenSua, trongLuong, donGia, tpDinhDuong, loiIch, hinh);
            
            int result = SuaUtils.getDao().themSuaMoi(sua);
            request.setAttribute("result", result);
        }        

        request.getRequestDispatcher("views/them-sua-moi.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
