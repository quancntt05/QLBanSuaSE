package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.HangSuaUtils;
import businessLogics.LoaiSuaUtils;
import businessLogics.SuaUtils;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/TimKiemSuaServlet")
public class TimKiemSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TimKiemSuaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        List<HangSua> dsHangSua = SuaUtils.getDao().dsHangSua();
        List<LoaiSua> dsLoaiSua = SuaUtils.getDao().dsLoaiSua();

       
        request.setAttribute("loaiSua", dsLoaiSua);
        request.setAttribute("hangSua", dsHangSua);
        
        if(request.getParameter("btnTimKiem") != null) {
            String loaiSua = request.getParameter("cboLoaiSua");
            String hangSua = request.getParameter("cboHangSua");
            String key = request.getParameter("txtTenSuaTim");
            
            HangSua hangSua_ = HangSuaUtils.getDao().findHangSuaById(hangSua);
            LoaiSua loaiSua_ = LoaiSuaUtils.getDao().findLoaiSuaById(loaiSua);
            List<Sua> lSua = SuaUtils.getDao().findSuaByKey(loaiSua_, hangSua_, key);
            
            request.setAttribute("lSua", lSua);
            request.setAttribute("sosp", lSua.size());
        }
        request.getRequestDispatcher("views/tim-kiem-sua.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
