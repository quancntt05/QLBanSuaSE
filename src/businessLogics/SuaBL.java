package businessLogics;

import java.util.List;

import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

public interface SuaBL {
	List<Sua> dsSua();
	List<Sua> dsSuaTheoPhanTrang(List<Sua> dsSua, int start, int limit);
	List<HangSua> dsHangSua();
	List<LoaiSua> dsLoaiSua();
	List<Sua> dsSuaTheoLoaiSua(String maLoaiSua);
	List<Sua> dsSuaTheoHangSua(String maHangSua);
	List<Sua> findSuaByKey(LoaiSua loaiSua, HangSua hangSua, String key);
	Sua findSuaById(String id);
	List<Sua> dsSuaBanChay();
	int themSuaMoi(Sua sua);
	
}
