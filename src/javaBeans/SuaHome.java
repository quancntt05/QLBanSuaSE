package javaBeans;
// Generated Mar 25, 2018 7:46:33 PM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class Sua.
 * @see javaBeans.Sua
 * @author Hibernate Tools
 */
@Stateless
public class SuaHome {

	private static final Log log = LogFactory.getLog(SuaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Sua transientInstance) {
		log.debug("persisting Sua instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Sua persistentInstance) {
		log.debug("removing Sua instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Sua merge(Sua detachedInstance) {
		log.debug("merging Sua instance");
		try {
			Sua result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sua findById(String id) {
		log.debug("getting Sua instance with id: " + id);
		try {
			Sua instance = entityManager.find(Sua.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
